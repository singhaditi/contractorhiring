<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">



<title>Insert title here</title>
<style>

    a { color: #FF0000; } /* CSS link color */

body {
    background-image:
        url("../images/spec21.jpg");
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-size: 100% 100%;
}

</style>
</head>
<body>
 <a href="/backtohome"><p align="right">BACK</p></a><br>

<center>
<!-- <button type="button" onclick="/Login">LOGOUT</button>
 -->
 <table border="4">
 			<tr>
				<th>User Id</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>DOB</th>
				<th>Gender</th>
				<th>Budget</th>
				<th>Duration</th>
				<th>Working Hours</th>
			</tr>
				      <td>${messages[0]}</td>
				      <td>${messages[1]}</td>
				      <td>${messages[2]}</td>
				      <td>${messages[3]}</td>
				      <td>${messages[4]}</td>
				      <td>${messages[5]}</td>
				      <td>${messages[6]}</td>
				      <td>${messages[7]}</td>
			</tr>       
	</table>
</center>	

 <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>	



</body>
</html>