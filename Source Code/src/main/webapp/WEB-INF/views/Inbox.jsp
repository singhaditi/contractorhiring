<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<title>See Your Messages</title>

<style>
#t {
    border-collapse: separate;
    border-spacing: 20px 15px;
}




body {
    background-image:
        url("../images/spec35.jpg");
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-size: 100% 100%;
}

#mydiv {
    position: fixed;
    top: 45%;
    left: 35%;
    transform: translate(-50%, -50%);
}





h1 {
  text-decoration: underline;
  color: black;
   text-shadow: 3px 2px white; 
}


    a { color: #FF0000; } /* CSS link color */
  

</style>
</head>
<body>
<!-- <button type="button" onclick="/Login">LOGOUT</button>
 -->
 <a href="/backtohome"><p align="right">BACK</p></a><br>
 <div id ="mydiv">
 <div class="container-fluid">

 <center>
 <div>
 
 
 <!-- <table id="t">
		 <c:forEach var="m" items="${message}">
		       	<tr>	
				      <td><h1>${m}</h1></td>
				  </tr>    
			
		</c:forEach> 
	</table>      -->


	
<table border="4" id="t">
 			<tr>
				<th><h1>Messages</h1></th>
			</tr>
			
			<tr>
				      <td><h3>${message[0]}</h3></td>
			</tr>
			<tr>
				      <td><h3>${message[1]}</h3></td>
			</tr>      
</table>

<center><h3>${messages}</h3></center>	
	
	
	
	
</div>	
</center>
</div>
</div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>	
	
	
</body>
</html>