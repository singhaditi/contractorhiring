<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Contractor Specification</title>
<style>


.container {

width: 500px;
  border-radius: 5px;
  background-color: white;
  padding: 20px;
}


body {
background-color:powderblue;



background-repeat: no-repeat;
background-attachment: fixed;

}

h1 {
  text-decoration: underline;
  color: white;

}

</style>

</head>
<body>
<div class="container" style="background-color:green; width:50px ; height:10px;">
<a href="/Login">LOGOUT</a></div>

<h1 text align="center">Contractor Specifications</h1>
<form:form action="/message" method="post" modelAttribute = "specificationDetails">

<center>
              <div class="container"><table>
              <tr>
              	<td><form:label path="budget">Budget</form:label></td>
              	<td><form:input path="budget" id="budget"  required="true" /></td>
				<td><form:errors path="budget" cssClass="error" /></td>
				
				</tr>
				<tr>
              	<td><form:label path="duration">Duration</form:label></td>
              	<td><form:input path="duration" id="duration"  required="true" /></td>
				<td><form:errors path="duration" cssClass="error" /></td>
				</tr>
				<tr>
				<td><form:label path="hours">Working Hours</form:label></td>
              	<td><form:input path="hours" id="hours" /></td>
				<td><form:errors path="hours" cssClass="error" /></td>
				</tr>
				 <tr>
				<td><form:label path="userid">User Id</form:label></td>
              	<td><form:input path="userid" id="userid" list="users" required="true" />
              	<datalist id="users">
              	<c:forEach var="u" items="${users}">
              	<option>
              	${u['user_id']}
              	</option>
              	</c:forEach>
              	</datalist>
              	</td>
				<td><form:errors path="userid" cssClass="error" /></td>
				</tr>
				<tr>
				<td><input type="submit" value="Submit" id="submit"
					name="submit"></td>
					<td><input type="reset" value="reset" id="reset"
					name="reset"></td>
				
			</tr>
			
		</table></div>
	</center>
</form:form>
</body>
</html>