package com.example.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;


public class SpecificationDetails {
        @Min(value=1000, message="must be greater than 1000")
	@Max(value=1000000, message="must be less than 1000000")
	private int budget;

        @Min(value=1, message="must be greater than 1")
	@Max(value=80, message="must be less than 80")
	private int duration;


        @Min(value=1, message="must be greater than 1")
	@Max(value=18, message="must be less than 18")
	private int hours;
        
    @NotEmpty(message="must not be empty")  
	private String user_id;

	public int getBudget() {
		return budget;
	}

	public void setBudget(int budget) {
		this.budget = budget;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}
	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}

	public String getUserid() {
		return user_id;
	}

	public void setUserid(String user_id) {
		this.user_id = user_id;
	}

}
