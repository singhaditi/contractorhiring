package com.example.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Component;

@Component
public class User {
	

	@Size(min = 1, message = "Please enter your first name.")
//	@Pattern(regexp = "[A-Z][a-zA-Z]*")
        private String firstName;
	//private String status;

	@Size(min = 1, message = "Please enter your last name.")
        @Pattern(regexp = "[a-zA-z]+([ '-][a-zA-Z]+)*")
	private String lastName;

	@Pattern(regexp = "(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/[0-9]{4}", message ="Please enter valid date of birth. In the format dd/mm/yyyy.")
	private String DoB;

	@NotEmpty(message="Please fill valid input.")
	private String gender;

	@NotNull
	//@Pattern(regexp= "^[a-z0-9_-@]{3,16}$" )
	@Size(min=1, message="Please Enter Valid UserId.The UserId can have alphabet,number,@,-,_ only")
	private String userId;

        @NotNull
	@Size(min = 6,message = "Please enter your password.")
//	@Pattern(regexp="^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{4,}$")
	private String Password;

	private String questionType;

	@NotNull
        @Size(min=1, message="Please Enter your security answer, minimum Length must be 1 and must be an alphabet.")
	//@Pattern(regexp="[A-Za-z]")
	private String answer;

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/*
	 * public String getStatus() { return status; } public void setStatus(String
	 * status) { this.status = status; }
	 */
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getDoB() {
		
		return DoB;
	}
	public void setDoB(String DoB) {
		this.DoB = DoB;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	public String getQuestionType() {
		return questionType;
	}
	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	@Override
	public String toString() {
		return "User [firstName=" + firstName + ", lastName=" + lastName + ", DoB=" + DoB
				+ ", gender=" + gender + ", userId=" + userId + ", Password=" + Password + ", questionType="
				+ questionType + ", answer=" + answer + "]";
	}
	

	

}
