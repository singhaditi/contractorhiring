package com.example.model;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Component;

@Component
public class Password {
	@Size(min = 6,message = "Please update the highlighted mandatory(*) fields.")
	@Pattern(regexp="^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{4,}$")
	private String newpassword;
	private String repassword;

	public Password() {
		super();
	}

	public Password(String newpassword, String repassword) {
		super();
		this.newpassword = newpassword;
		this.repassword = repassword;
	}

	public String getNewpassword() {
		return newpassword;
	}

	public void setNewpassword(String newpassword) {
		this.newpassword = newpassword;
	}

	public String getRepassword() {
		return repassword;
	}

	public void setRepassword(String repassword) {
		this.repassword = repassword;
	}
}
