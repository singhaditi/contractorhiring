package com.example.Contractor;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.Dao.UserDao;
import com.example.Dao.UserDaoImpl;

@Controller
public class MessageController {
	
	@Autowired
	UserDaoImpl service;
	

	@RequestMapping("/typemessage")
	public String typemessage(){
		return "typemessage";
	}
	
	
	
	@ModelAttribute("useridList")
	public List<String> userIdList(){
		return service.getAllUserId();
	}
	
}
