package com.example.Contractor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import com.example.Dao.UserDaoImpl;
import com.example.model.Login;
import com.example.model.Password;
import com.example.model.SpecificationDetails;
import com.example.model.User;

@Controller
@SessionAttributes("user_id")
public class RegistrationController {
	@Autowired
	private UserDaoImpl userDaoImpl;
	public static String userid;
	@RequestMapping(value="/Login",method=RequestMethod.GET)
	public String login(@ModelAttribute("login") Login login,ModelMap model)
	{
		model.addAttribute("login", new Login());
		return "Login";	
	}
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login() {
		//model.addAttribute("login", new Login());
	return "Login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String homePage(@ModelAttribute("login") Login login, ModelMap model) {

		model.put("user_id",login.getUserId());
		 userid=login.getUserId();
		if (userDaoImpl.userExist(login)) {
			if (login.getUserType().equals("Admin"))
				return "AdminAction";
			else if (login.getUserType().equals("User")) {
				model.put("user_id", login.getUserId());
				return "UserAction";

			}

		}
		model.put("error", "Invalid input");
		return "Login";
	}
	
	@RequestMapping(value="/Inbox",method=RequestMethod.GET)
	public String userInbox(@ModelAttribute("user_id")String user_id,ModelMap model) {
		List<String> msg=userDaoImpl.getMessages(user_id);
		if(msg!=null){
			{model.addAttribute("message", msg);}
		System.out.println(msg);
		}
			else {
		model.put("message","Please Fill the form");}
		return "Inbox";
	}
	
	@RequestMapping(value="/Registration", method=RequestMethod.GET)
	public String userRegistrationDisplay(@ModelAttribute("register") User register) {
		return "Registration";
	}
	
	@RequestMapping(value="/SpecificationsDetails", method=RequestMethod.GET)
	public String specificationDetails(
			@ModelAttribute("specificationDetails") SpecificationDetails specificationDetails,ModelMap map) {
		map.addAttribute("users",userDaoImpl.showDetails());
		return "SpecificationsDetails";
	}
	
	@RequestMapping(value="/message",method=RequestMethod.POST)
	public String message(@ModelAttribute("specificationDetails") @Valid SpecificationDetails specificationDetails, BindingResult bindingResult,ModelMap model)
	{
		if(bindingResult.hasErrors()){
			return "SpecificationsDetails";
		}
		
		if(userDaoImpl.enterSpecification(specificationDetails)) {
			model.put("message","Form saved Successfully");
			return "message";
		}else if(!userDaoImpl.enterSpecification(specificationDetails)){
			model.put("message", "You are either not registered or already filled the form!!!");
			return "SpecificationsDetails";
		}
		model.put("message","Form not saved");
		return "message";
	}
	
	
	@RequestMapping(value="/Register",method=RequestMethod.POST)
	public String register(@ModelAttribute("register") @Valid User register, BindingResult bindingResult,ModelMap model)
	{
		 
		if (bindingResult.hasErrors())
		{
			return "Registration";
		}
		
		if(userDaoImpl.addUser(register)) {
			model.put("status", "Registeration done Successfully");
		}
		else {
			model.put("status", "User Id is already used");
		}
		return "Success";
	}
	
	
	@RequestMapping(value="/Success",method=RequestMethod.POST)
	public String success(@ModelAttribute("register") @Valid User register, BindingResult bindingResult,ModelMap model)
	{
		if (bindingResult.hasErrors())
		{
			return "Registration";
		}
		
		return "Success";
	} 
	
	@RequestMapping(value="/ForgetPassword" ,method=RequestMethod.GET)
	public String goToForgetPassword(@ModelAttribute("register") User login,BindingResult result,ModelMap model){
		return "forgetPassword";
	}
	
	@RequestMapping(value="/ForgetPassword",method=RequestMethod.POST)
	public String forgetPassword(@ModelAttribute("register") User login,BindingResult result,ModelMap model)
	{		
		
		/*if(result.hasErrors())
		{   model.put("error", "plz provide correct credentials");
			return "forgetPassword";
		}*/
		//System.out.println(userid+" "+answer);
		System.out.println(login);
		User user=userDaoImpl.checkQuestionAnswer(login.getUserId(), login.getQuestionType(), login.getAnswer());
		model.put("user_id", login.getUserId());
		System.out.println(user);
		if(user!=null)
		{	//userid=user.getUserId();
			return "redirect:/resetPassword";
		}
		model.put("error", "plz provide correct credentials!");
		return  "forgetPassword";	
	}
		@RequestMapping(value="/resetPassword" ,method=RequestMethod.GET) 
		public String resetPassword1(@ModelAttribute("reset") Password password, ModelMap model)
		{	model.addAttribute("login", new Login());
			
		return 	"ResetPassword";
		}
	    @RequestMapping(value="/resetPassword" ,method=RequestMethod.POST) 
		public String resetPassword(@ModelAttribute("reset") @Valid Password password,BindingResult result, ModelMap model) {
	    	if(result.hasErrors())
	    	{
	    		model.put("error", "plz enter stong password and no special character allowed !");
	    		return "ResetPassword";
	    	}
	    	System.out.println(password.getNewpassword()+" "+password.getRepassword());
	    	System.out.println(model.getAttribute("user_id"));
		 if(userDaoImpl.checkPassword(password.getNewpassword(), password.getRepassword(),(String) model.getAttribute("user_id"))) {
			 
			 model.put("msg","your password is successfully updated!");
			 return "redirect:/Login"; 
		 }
		 else
		 {
			 model.put("error", "password mismatch! \n try again!");
			 return "ResetPassword";
		 }
		 
	    }
	    @RequestMapping(value="/viewDetails" ,method=RequestMethod.GET)
		public String viewDetailsUser(ModelMap model){
	    	List<Object> msg=userDaoImpl.getUserDetails(userid);
	    	System.out.println(msg);
	    	model.put("messages", msg);
			return "viewDetails";
		}
	/*
	 * @RequestMapping(value="/viewDetails" ,method=RequestMethod.POST) public
	 * String viewDetailsUser(@ModelAttribute("register") User login,BindingResult
	 * result,ModelMap model){ return "forgetPassword"; }
	 */
		 
	@ModelAttribute("questionList")
	public  Map<String,String> buildState(){
		Map<String, String> map=new HashMap<String, String>();
		map.put("What primary school did you attend?", "What primary school did you attend?");
		map.put("What Is your favorite book?", "What Is your favorite book?");
		map.put("What is your favorite food?", "What is your favorite food?");
		return map;
	}
	@RequestMapping(value="/pendingrequest")
	public String showDetails(ModelMap model){
		List<Map<String,Object>> list=userDaoImpl.showDetails();
		List<Object> l=new ArrayList<>();
		for(Map<String,Object> map:list)
		{    for(Object key:map.keySet())
			l.add(map.get(key));
		}
		//ModelMap model=new ModelMap();
		model.addAttribute("list",list);
		System.out.println(l);
		return "pendingrequest";
	}
	
	@ModelAttribute("usertype")
	public List<String> radio() {
		List<String> list = new ArrayList<>();
		list.add("Admin");
		list.add("User");
		return list;
	}
	
	@ModelAttribute("services")
	public List<String> select() {
		List<String> list = new ArrayList<>();
		list.add("Part Time");
		list.add("Full Time");
		return list;
	}
	
	@RequestMapping(value="/logout",method= RequestMethod.GET)
	public String logoutUser(ModelMap model){
		model.remove("user_id");
		return "Login";
	}
	
	@RequestMapping(value="/backtohome",method= RequestMethod.GET)
	public String backtohome(ModelMap model){
		//model.remove("user_id");
		return "UserAction";
	}
	
	@RequestMapping(value="/backtoadminhome",method= RequestMethod.GET)
	public String backtoadminhome(ModelMap model){
		//model.remove("user_id");
		return "AdminAction";
	}
	
}

